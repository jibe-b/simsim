# Contribution guide

Please report bugs and feature requests in the [issue tracker](https://gitlab.com/jibe-b/simsim/issues).

Feel free to ping me on twitter: [@jibe_jeybee](https://twitter.com/jibe_jeybee).
